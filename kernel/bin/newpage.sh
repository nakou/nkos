#!/bin/bash
appfile="$2"".ApplicationClass.php" 
storefile="$2""Store.StoreClass.php" 
templatefile="$2"".Template.php"
defaultapp="<?php class "$2" extends Applications{}"
defaultstr="<?php class "$2"Store extends Store{	public function run()	{return $2	}}"
defaulttpl="<div class="container"><div class="hero-unit"><h1>Hi, BITCH!</h1><p>Result : <?php var_dump($StorageArray);?></p></div></div>"

if [ $1 != "-h" ] || [ $1 != "--help" ]
then
	if [ $1 = "-o" ]
	then
		echo "Création d'une page de type Post..."
		echo $defaultapp > ../../applications/posts/$appfile
		echo $defaulttpl > ../../graphicals/posts/$templatefile
		echo $defaultstr > ../../storages/posts/$storefile
	elif [ $1 = "-r" ]
	then
		echo "Création d'une page de type Private..."
		echo $defaultapp > ../../applications/private/$appfile
		echo $defaulttpl > ../../graphicals/private/$templatefile
		echo $defaultstr > ../../storages/private/$storefile
	elif [ $1 = "-r" ]
	then
		echo "Création d'une page de type Public..."
		echo $defaultapp > ../../applications/public/$appfile
		echo $defaulttpl > ../../graphicals/public/$templatefile
		echo $defaultstr > ../../storages/public/$storefile
	else
		echo "Bad argument"
	fi
else
	echo "Aide au script de création de page :"
	echo "--help, -h  Aide"
	echo "-o Page de type post (uniquement accessible en formulaire)"
	echo "-u Page de type public (accessible a tous)"
	echo "-r Page de type privé (accessible uniquement a quelqu'un de connecté en session)"
fi
