<?php
// Menu Module
$lang['title'] = 'TITLE!';

// Errors Codes

$lang[0] = 'Sorry, we don\'t know this User...';
$lang[1] = 'This keyspace doesn\'t exist!';
$lang[2] = 'We encounter a problem during the creation of the KeySpace.';
$lang[3] = 'You have not selected a valid keyspace...';
$lang[4] = 'We encounter a problem during the request...';
$lang[404] = 'Error 404 : Sorry, but this page doesn\'t exist!';
$lang[418] = 'Error 418 : I\'m a teapot!';