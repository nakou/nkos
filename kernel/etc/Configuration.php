<?php 
class Conf
{
	static $underFiles = 3;
	static $DefaultLang = 'FR_Francais';

	static $dbSQL = array('type' => 'mysql',
						   'server' => 'localhost',
						   'dbname' => 'nakoudotnet',
						   'login' => 'nakoudotnet',
						   'passwd' => 'nakoudotnet' );

	static $LangsList = array();

	static $lang = array();

	static public function defineLang($lang)
	{
		Conf::$lang = $lang;
	}

	static public function defineLangList($langlist)
	{
		Conf::$LangsList = $langlist;
	}
}