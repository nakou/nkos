<?php
// Variables d'environement graphique.
define("ABS",dirname(dirname($_SERVER['SCRIPT_NAME']))."/"); // A MODIFIER SUR CHAQUE DEPLOIEMENT
define("RESSOURCES",ABS."ressources/");
define("IMGS",RESSOURCES."img/");
define("AVATARS",IMGS."avatars/");
define("ITEMS",IMGS."items/");
define("CSS",RESSOURCES."css/");
define("JS",RESSOURCES."js/");


// Variables d'environement système.
define("ROOT",dirname(dirname(dirname(__FILE__))).'/');
define("KERNEL",ROOT."kernel/");
define("ETC",KERNEL."etc/");
define("LIB",KERNEL."lib/");
define("APPS",ROOT."applications/");
define("STORE",ROOT."storages/");
define("GRAPHS",ROOT."graphicals/");
define("APMOD",APPS."modules/");
define("GRAMOD",GRAPHS."modules/");
define("LANG",ETC."langs/");

$Privates = array();
$Publics = array();
$Posts = array();


	// Chargement des fichier de configurations.
$dirname = ETC;
$dir = opendir($dirname);
while($file = readdir($dir)) {
	if($file != '.' && $file != '..' && !is_dir($dirname.$file) && $file != '.htaccess')
	{
		require_once($dirname.$file);
	}
}
closedir($dir);

	// Chargement de la langue à partir du Cookie.

if(isset($_SESSION['lang']))
{
	require_once(LANG.$_SESSION['lang'].'.php');	
}
elseif(!empty($_COOKIE['lang']))
{
	if(file_exists(LANG.$_COOKIE['lang'].'.php'))
	{
		require_once(LANG.$_COOKIE['lang'].'.php');	
	}
	else
	{
		setcookie('lang',Conf::$DefaultLang);
		require_once(LANG.$_COOKIE['lang'].'.php');
	}
}
else
{
	require(LANG.Conf::$DefaultLang.'.php');
}

	require(LANG.'LangsList.php');

Conf::defineLang($lang); // On fixe la langue dans les variables de configuration.
Conf::defineLangList($LangList); // On charge la liste des langues disponnibles.

	// Chargement des classes kernel.
$dirname = KERNEL;
$dir = opendir($dirname); 
while($file = readdir($dir)) {
	if($file != '.' && $file != '..' && $file != '.htaccess' && $file != 'etc' && $file != 'index.php' && $file != 'lib' && $file != 'bin' && $file != 'Store.KernelClass.php')
	{
		require_once($dirname.$file);
	}
}
require_once($dirname.'Store.KernelClass.php'); // Le jour ou PHP traitera les fichiers dans l'ordre, on pourra retirer ça, et la condition du if..
closedir($dir);

	// Chargement des librairies externes
$dirname = LIB;
$dir = opendir($dirname); 
while($file = readdir($dir)) {
	if($file != '.' && $file != '..')
	{
		require_once($dirname.$file);
	}
}
closedir($dir);

/**
/ CHARGEMENT DES APPLICATIONS
**/


	// Chargement des applications publiques. (Contenu Non connecté/connecté.)
$dirname = APPS."/public/";
$dir = opendir($dirname); 
while($file = readdir($dir)) {
	if($file != '.' && $file != '..' && $file != '.htaccess' && $file != 'modules' && $file != 'index.php')
	{
		require_once($dirname.$file);
		$Publics[] = Router::getClassName($file);
	}
}
closedir($dir);

	// Chargement des applications privées. (Contenu connecté)
$dirname = APPS."/private/";
$dir = opendir($dirname); 
while($file = readdir($dir)) {
	if($file != '.' && $file != '..' && $file != '.htaccess' && $file != 'modules' && $file != 'index.php')
	{
		require_once($dirname.$file);
		$Privates[] = Router::getClassName($file);
	}
}
closedir($dir);

	// Chargement des applications posts.
$dirname = APPS."/posts/";
$dir = opendir($dirname); 
while($file = readdir($dir)) {
	if($file != '.' && $file != '..' && $file != '.htaccess' && $file != 'modules' && $file != 'index.php')
	{
		require_once($dirname.$file);
		$Posts[] = Router::getClassName($file);
	}
}
closedir($dir);


/**
/ CHARGEMENT DES MAGAZINS DE DONNEES
**/

$dirname = STORE."/public/";
$dir = opendir($dirname); 
while($file = readdir($dir)) {
	if($file != '.' && $file != '..' && $file != '.htaccess' && $file != 'modules' && $file != 'index.php')
	{
		require_once($dirname.$file);
	}
}
closedir($dir);

	// Chargement des applications privées. (Contenu connecté)
$dirname = STORE."/private/";
$dir = opendir($dirname); 
while($file = readdir($dir)) {
	if($file != '.' && $file != '..' && $file != '.htaccess' && $file != 'modules' && $file != 'index.php')
	{
		require_once($dirname.$file);
	}
}
closedir($dir);


	// Chargement des applications posts.
$dirname = STORE."/posts/";
$dir = opendir($dirname); 
while($file = readdir($dir)) {
	if($file != '.' && $file != '..' && $file != '.htaccess' && $file != 'modules' && $file != 'index.php')
	{
		require_once($dirname.$file);
	}
}
closedir($dir);


/**
/ CHARGEMENT DU MAGAZIN DE DONNEE COMMUN
**/

require_once(STORE.'/MasterStorage.KernelClass.php');

/**
* CHARGEMENT DU GESTIONNAIRE DE MODULE
**/

require_once(STORE.'/FeedModuleStorage.uKernelClass.php');

	// Chargement des micro-classes modules.
$dirname = APMOD;
$dir = opendir($dirname); 
while($file = readdir($dir)) {
	if($file != '.' && $file != '..' && $file != '.htaccess' && $file != 'modules' && $file != 'index.php')
	{
		require_once($dirname.$file);
	}
}
closedir($dir);

	// Lancement de l'Engine.
?>