<?php 
class Engine
{
	private $PublicList;
	private $PrivateList;
	private $PostsList;
	private $ApplicationName;
	private $ApplicationObj;
	private $ApplicationStatus;
	private $StorageObj;
	private $Specificity = NULL;
	private $SpecIsArray = false;

	public function __construct($Publics, $Privates, $Posts)
	{
		$this->PublicList = $Publics;
		$this->PrivateList = $Privates;
		$this->PostsList = $Posts;
		$ParsedURL = Router::parse($_SERVER['REDIRECT_URL'],Conf::$underFiles);
		$this->defineAppName($ParsedURL[0],$ParsedURL[1]);
		$this->ApplicationStatus = $this->defineStatus($this->ApplicationName);
		$this->ApplicationObj = new $this->ApplicationName($this->ApplicationName,$this->ApplicationStatus);
		$this->StorageObj = new Storage($this->ApplicationName,$this->Specificity);
		$this->StorageMod = new FeedModuleStorage($this->StorageObj->getDB());		
		if($this->StorageObj->getError())
		{
			$this->Specificity = $this->StorageObj->getErrorCode();
			$this->ApplicationName = 'errors';
			$this->ApplicationStatus = $this->defineStatus($this->ApplicationName);
			$this->ApplicationObj = new $this->ApplicationName($this->ApplicationName,$this->ApplicationStatus);
			$this->StorageObj = new Storage($this->ApplicationName,$this->Specificity);
		}
		$this->ApplicationObj->render($this->StorageObj->getVars(),$this->StorageMod->getVars());
		$this->debugvars();
	}

	/**
	* defineAppName : Permet de prendre l'URL et d'en tirer les application appelées.
	* @param $AppName : Nom de l'application.
	* @param $Specifs : Spécifications, si il y a.
	**/

	public function defineAppName($AppName,$Specifs)
	{
		// On regarde si l'on cherche un user.
		if(isset($AppName))
		{
			if(isset($Specifs))
			{
				if(is_array($Specifs))
				{
					$this->SpecIsArray = true;
				}
				$this->Specificity = $this->SecuriseSpecif($Specifs);
			}

			if(empty($_SESSION))
			{
				if(empty($_POST))
				{
					if(class_exists($AppName) && in_array($AppName, $this->PublicList))
					{
						$this->ApplicationName = $AppName;
					}
					else
					{
						$this->ApplicationName = 'errors';
						$this->Specificity = 404;
					}						
				}
				else
				{
					if(class_exists($AppName) && (in_array($AppName, $this->PublicList) || in_array($AppName, $this->PostsList)))
					{
						$this->ApplicationName = $AppName;
					}
					else
					{
						$this->ApplicationName = 'errors';
						$this->Specificity = 404;
					}						
				}			
			}
			else
			{
				if(empty($_POST))
				{
					if(class_exists($AppName) && (in_array($AppName, $this->PublicList) || in_array($AppName, $this->PrivateList)))
					{
						$this->ApplicationName = $AppName;
					}
					else
					{
						$this->ApplicationName = 'errors';
						$this->Specificity = 404;
					}							
				}
				else
				{
					if(class_exists($AppName) && (in_array($AppName, $this->PublicList) || in_array($AppName, $this->PrivateList) || in_array($AppName, $this->PostsList)))
					{
						$this->ApplicationName = $AppName;
					}
					else
					{
						$this->ApplicationName = 'errors';
						$this->Specificity = 404;
					}							
				}	
			}
		}
		else
		{
			if(empty($_SESSION))
			{
				$this->ApplicationName = 'home';
			}
			else
			{
				$this->ApplicationName = 'myhome';
			}
		}
	}

	public function defineStatus($AppName)
	{
		if(in_array($AppName, $this->PublicList))
		{
			return 'public';
		}
		if(in_array($AppName, $this->PrivateList))
		{
			return 'private';
		}
		if(in_array($AppName, $this->PostsList))
		{
			$this->securiseIt();
			return 'posts';
		}
	}

	public function securiseIt()
	{
		foreach ($_POST as $key => $value) {
			$value = addslashes($value);
		}
	}

	public function SecuriseSpecif($Specifs)
	{
		if(is_array($Specifs))
		{
			foreach ($Specifs as $value) {
				$value = $value = addslashes($value);
			}
		}
		else
		{
			$Specifs = addslashes($Specifs);
		}
		return $Specifs;
	}

	/**
	* debugvars : Affiche les valeurs de debug pour la classe.
	**/

	public function debugvars()
	{
		echo '<pre>';
		echo 'NKOS v0.9 Release Candidate</br>';
		echo 'ApplicationName : </br>';
		echo var_dump($this->ApplicationName);
		echo 'Specificity : </br>';
		echo var_dump($this->Specificity);
		echo 'ApplicationObj : </br>';
		echo var_dump($this->ApplicationObj);
		echo 'StorageObj : </br>';
		echo var_dump($this->StorageObj);
		echo 'VarsPOST : </br>';
		echo var_dump($_POST);
		echo 'VarsSESSION : </br>';
		echo var_dump($_SESSION);
		echo 'Privates Apps : </br>';
		echo var_dump($this->PrivateList);
		echo 'Publics Apps : </br>';
		echo var_dump($this->PublicList);
		echo 'Posts Apps : </br>';
		echo var_dump($this->PostsList);
		echo '</pre>';
	}
}