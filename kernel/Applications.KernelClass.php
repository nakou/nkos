<?php 
class Applications
{
	private $AppStatus;
	private $NameOfApplication;
	private $rendered = false;

	public function __construct($Name,$Status)
	{
		$this->NameOfApplication = $Name;
		$this->AppStatus = $Status;
	}

	public function render($StorageArray,$ModsArray)
	{
		if($this->rendered)
			{return false;}
		ob_start();
		require(GRAPHS.$this->AppStatus.'/'.$this->NameOfApplication.'.Template.php');
		$content_template = ob_get_clean();
		require(GRAPHS.'Master.Template.php');
		$this->rendered = true;
	}
}