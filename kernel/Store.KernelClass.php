<?php
class Store extends Storage{

	private $db;
	private $StoreName;
	private $StoreOption;
	private $RunReturn;

	public function __construct($DB,$Name,$Option)
	{
		$this->db = $DB;
		$this->StoreName = $Name;
		$this->StoreOption = $Option;
		$this->RunReturn = $this->run();
	}

	public function run()
	{
		return false;
	}

	public function getdb()
	{
		return $this->db;
	}

	public function getRunReturn()
	{
		return $this->RunReturn;
	}

	public function getStoreOption()
	{
		return $this->StoreOption;
	}

	public function getStoreName()
	{
		return $this->StoreName;
	}
}