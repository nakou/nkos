<?php 
class Router
{
	/**
	* Parse : Permet de prendre l'URL et d'en tirer les application appelées.
	* @param $url : URL a découper.
	* @param $k : décalage à l'origine, en fonction de la position du site sur le serveur.
	* @return Tableau contenant l'application appelée, et l'élément complémentaire si il y en a un.
	**/

	static function parse($url,$k)
	{
		$results = array();
		$parms = explode('/', $url);
		$url_nbr_elements = count($parms);
		if(isset($parms[$k+1]))
		{
			if($url_nbr_elements <= $k+3)
			{
				$results[0] = $parms[$k+1];
				if(isset($parms[$k+2]))
				{
					$results[1] = $parms[$k+2];
				}
			}
			else
			{
				for($i = 0; $i < $url_nbr_elements - ($k + 1); $i++)
				{
					if($i == 0)
						$results[0] = $parms[$i + $k + 1];
					else
						{
							if(empty($parms[$i + $k + 1]))
							{
								$i = $url_nbr_elements;
							}
							else
								$results[1][] = $parms[$i + $k + 1];							
						}
				}
			}
		}
		if(empty($results[0]))
		{
			$results[0] = NULL;	
		}
		if(empty($results[1]))
		{
			$results[1] = NULL;	
		}
		else
		{
			// On vérifi qu'on a pas rempli un tableau que pour un seul élément à cause d'un / en trop dans l'URL
			if(is_array($results[1]))
			{
				if(count($results[1]) <= 1)
				{
					$temp = $results[1][0];
					$results[1] = NULL;
					$results[1] = $temp;
				}
			}
		}
		return $results;
	}

	static function getClassName($fileName)
	{
		$classname = explode('.', $fileName);
		return $classname[0];
	}
}
