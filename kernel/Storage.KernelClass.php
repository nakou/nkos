<?php 
class Storage
{
	private $error = false;
	private $errorCode = NULL;
	private $Vars;
	private $db;
	private $StorageClassReference = NULL;
	private $MasterStorageClass;

	public function __construct($NameOfMethod,$Option=NULL)
	{
		$dsn = Conf::$dbSQL['type'].':host='.Conf::$dbSQL['server'].';dbname='.Conf::$dbSQL['dbname'];
		$this->db = new PDO($dsn,Conf::$dbSQL['login'],Conf::$dbSQL['passwd']);
		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		$this->MasterStorageClass = new MasterStorage($this->db);
		if(!method_exists($this->MasterStorageClass, $NameOfMethod) && !class_exists($NameOfMethod.'Store'))
		{
			$this->Vars = $this->defaultMethod($NameOfMethod);
		}
		else
		{
			if(method_exists($this->MasterStorageClass, $NameOfMethod))
			{
				$this->Vars = $this->MasterStorageClass->$NameOfMethod($Option);
				$this->error = $this->MasterStorageClass->getError();
				$this->errorCode = $this->MasterStorageClass->getErrorCode();
			}

			$ClassName = $NameOfMethod.'Store';

			if(class_exists($ClassName))
			{
				$this->StorageClassReference = new $ClassName($this->db,$NameOfMethod,$Option);
				$this->Vars = $this->StorageClassReference->getRunReturn();
				$this->error = $this->StorageClassReference->getError();
				$this->errorCode = $this->StorageClassReference->getErrorCode();				
			}
		}			
	}

	public function getVars()
	{
		return $this->Vars;
	}

	public function getDB()
	{
		return $this->db;
	}

	public function defaultMethod($type)
	{
		return $type;
	}

	public function getError()
	{
		return $this->error;
	}

	public function getErrorCode()
	{
		return $this->errorCode;
	}

	public function setError($status)
	{
		$this->error = $status;
	}

	public function setErrorCode($code)
	{
		$this->errorCode = $code;
	}

/*
*
*
*	METHODES UTILITAIRES
*
*/


	public function add($TableName, $TableSpec, $TableEntry)
	{
		if(!is_array($TableSpec) || !is_array($TableEntry))
			return false;
		if(empty($TableName) || empty($TableSpec) || empty($TableEntry))
			return false;
		if(count($TableSpec) != count($TableEntry))
			return false;

		$TableElements = '';
		for($i = 0; $i < count($TableSpec); $i++)
		{
			if($i != 0)
			{
				$TableElements .= ' ,';
			}
			$TableElements .= $TableSpec[$i];
		}

		$TablePrepInsert = '';
		for($i = 0; $i < count($TableEntry); $i++)
		{
			if($i != 0)
			{
				$TablePrepInsert .= ' ,';
			}
			$TablePrepInsert .= $TableEntry[$i];
		}

		try
		{
			$req = $this->getdb()->query('INSERT INTO '.$TableName.'('.$TableElements.') VALUES('.$TablePrepInsert.')');
			return true;
		} 
		catch(PDOException $e)
		{
			$this->setError(true);
			$this->setErrorCode(600);
			return false;
		}
	}

	public function modify($TableName, $TableSpec, $TableEntry, $KeysNeeds, $KeysValues)
	{
		if(!is_array($TableSpec) || !is_array($TableEntry) || !is_array($KeysNeeds) || !is_array($KeysValues))
			return false;

		if(empty($TableName) || empty($TableSpec) || empty($TableEntry) || empty($KeysNeeds) || empty($KeysValues))
			return false;

		if(count($TableSpec) != count($TableEntry))
			return false;

		if(count($KeysNeeds) != count($KeysValues))
			return false;

		for($i = 0; $i < count($TableSpec); $i++)
		{
			if($i != 0)
			{
				$SetString .= ' ,';
			}
			$SetString .= '`'.$TableSpec[$i].'` = '.$TableEntry[$i];
		}

		for($i = 0; $i < count($KeysNeeds); $i++)
		{
			if($i != 0)
			{
				$KeysString .= ' AND';
			}
			else
			{
				$KeysString .= 'WHERE ';
			}
			$KeysString .= ' `'.$KeysNeeds[$i].'` = '.$KeysValues[$i];
		}

		try
		{
			$req = $this->getdb()->query('UPDATE '.$TableName.' SET '.$SetString.' '.$KeysString.'');
			return true;
		} 
		catch(PDOException $e)
		{
			$this->setError(true);
			$this->setErrorCode(601);
			return false;
		}
	}

	public function delete($TableName, $KeysNeeds, $KeysValues)
	{
		if(!is_array($KeysNeeds) || !is_array($KeysValues))
			return false;

		if(empty($TableName) || empty($KeysNeeds) || empty($KeysValues))
			return false;

		if(count($KeysNeeds) != count($KeysValues))
			return false;

		for($i = 0; $i < count($KeysNeeds); $i++)
		{
			if($i != 0)
			{
				$KeysString .= ' AND';
			}
			else
			{
				$KeysString .= 'WHERE ';
			}
			$KeysString .= ' `'.$KeysNeeds[$i].'` = '.$KeysValues[$i];
		}

		try
		{
			$req = $this->getdb()->query('DELETE FROM '.$TableName.' '.$KeysString.'');
			return true;
		} 
		catch(PDOException $e)
		{
			$this->setError(true);
			$this->setErrorCode(602);
			return false;
		}
	}

	public function show($TableName, $TableSpec, $KeysNeeds = array(), $KeysValues = array())
	{
		if(!is_array($TableSpec) || !is_array($KeysNeeds) || !is_array($KeysValues))
			return false;

		if(empty($TableName) || empty($TableSpec) /*|| empty($KeysNeeds) || empty($KeysValues)*/)
			return false;

		if(count($KeysNeeds) != count($KeysValues))
			return false;

		$SelectString = '';
		for($i = 0; $i < count($TableSpec); $i++)
		{
			if($TableSpec[$i] == '*')
			{
				$SelectString = '*';
				$i = count($TableSpec);
			}
			else
			{
				if($i != 0)
				{
					$SelectString .= ' ,';
				}
				$SelectString .= '`'.$TableSpec[$i].'`';
			}
		}

		$KeysString = '';
		if(!empty($KeysNeeds) && !empty($KeysValues))
		{
			for($i = 0; $i < count($KeysNeeds); $i++)
			{
				if($i != 0)
				{
					$KeysString .= ' AND';
				}
				else
				{
					$KeysString .= 'WHERE ';
				}
				$KeysString .= ' `'.$KeysNeeds[$i].'` = '.$KeysValues[$i];
			}
		}

		try
		{
			$req = $this->getdb()->query('SELECT '.$SelectString.' FROM '.$TableName.' '.$KeysString.'');
			return $req->fetchAll();
		} 
		catch(PDOException $e)
		{
			$this->setError(true);
			$this->setErrorCode(603);
			return false;
		}
	}
}