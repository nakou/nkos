<?php
require_once('phpMailer/class.phpmailer.php');

$mail = new PHPMailer(true);

$mail->IsSMTP();

try {
  $mail->SMTPAuth   = true;
  $mail->SMTPSecure = "ssl";
  $mail->Host       = "smtp.gmail.com";
  $mail->Port       = 465;

	// Tes identifiants GMail
  $mail->Username   = "username@gmail.com";
  $mail->Password   = "passw0rd";

 	// Addresse de l'expéditeur (personnalisable)
  $mail->SetFrom('webmaster@monsite.com', 'Le Webmaster');

	// Destinataire, objet, corps
  $mail->AddAddress('thomas.graindorge@viacesi.fr');
  $mail->Subject = 'Test mail';
  $mail->MsgHTML("<b>Ici le corps du mail</b>");

  $mail->Send();
  echo "Message Sent OK</p>\n";

} catch (Exception $e) {
  echo $e->getMessage();
}
?>
