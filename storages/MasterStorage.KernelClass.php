<?php
class MasterStorage
{
	private $dbLink;
	private $error = false;
	private $errorCode = NULL;

	public function __construct($db)
	{
		$this->dbLink = $db;
	}

	public function home()
	{
		$_SESSION['login'] = 'lol';
		return 'home';
	}

	/*public function myhome()
	{
		return 'myhome';
	}*/

	public function errors($code=null)
	{
		if(isset($code))
		return $code;
		else
		return '404';
	}

	public function getError()
	{
		return $this->error;
	}

	public function getErrorCode()
	{
		return $this->errorCode;
	}

	public function setError($status)
	{
		$this->error = $status;
	}

	public function setErrorCode($Code)
	{
		$this->errorCode = $Code;
	}
}