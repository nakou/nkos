<?php
class FeedModuleStorage {
	private $dbLink;
	private $ContentArray;

	public function __construct($db){
		$this->dbLink = $db;
		$ListOfModules = get_class_methods($this);
		$Exclude = array('__construct','getVars');
		foreach ($ListOfModules as $Module) {
			if(!in_array($Module, $Exclude))
			$this->ContentArray[$Module] = $this->$Module();
		}
	}

	public function getVars()
	{
		return $this->ContentArray;
	}

	public function test()
	{
		return 'test';
	}
}